var connectionString = "postgres://zndrgpubihikgg:146c1377c6b8a39a937bc5f2795d5ce5ec8d5db478dfc8e162cb2eb379e83fc2@ec2-54-247-74-131.eu-west-1.compute.amazonaws.com:5432/d3gj63qhlmojhs";

var express = require('express');
var app = express();
const bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
const secToken = 'VGVjaE1hZ2ljLlByby1TYWxlc2ZvcmNlLURldnMuSGVyb2t1LjIxMDgyMDE4';

app.set( 'port', ( process.env.PORT || 5000 ));

app.listen( app.get( 'port' ), function() {
	console.log( 'Node server is running on port ' + app.get( 'port' ));
});

app.get("/", (req, res)=> { 
	if(req.header('Authorization') != secToken) { 
  		res.status(200).send('Non Authorized');
  		return;
	}
    
    //connect to DB Postgres
    const { Client } = require('pg');
    const client = new Client({
     	connectionString: connectionString,
    	ssl: true
    });
    client.connect();

    const offset = req.header('Offset');
    const limit = req.header('Limit');
    const search = req.header('Search');

    if (search == '') {
		const searchQuery = "SELECT * FROM cases \
                    OFFSET " + offset + " \
                    LIMIT " + limit + ";";
  		const countOfRecordsQuery = "SELECT Count(*) FROM cases";

  		//get all cases from Postgres
        client.query(searchQuery, (err, result) => {
      		if(err){
        		res.status(400).send(err);
        		client.end();
        		
        		return;
      		}

      		//get quantity of all cases in Postgres
      		client.query(countOfRecordsQuery, (countError, countResult) =>{
          		if(countError) {
            		res.status(400).send(countError);
            		client.end();
            		
            		return;
          		} 
          		const joinedResult = {records : result.rows, count : countResult.rows[0].count};
        		res.status(200).send(joinedResult);

          		client.end();
    		})
    	})
    } else {
    	const getTextFieldsQuery = "SELECT column_name FROM information_schema.columns WHERE (table_name = 'cases') AND (data_type = 'text' OR data_type='character varying');";

    	//get all the columns which will use to searching
    	client.query(getTextFieldsQuery, (textFieldsError, textFieldsResult) =>{
    		if(textFieldsError) {
    			res.status(400).send(textFieldsError);
    			client.end();
    			
    			return; 
    		}

    		const textFields = [];
			textFieldsResult.rows.forEach(field => {
				textFields.push(field.column_name);
			})
			const searchQueries = createSearchRecordsQueriesStr(textFields, search, offset, limit);

			//get all the found cases
			client.query(searchQueries.searchQueryStr, (searchQueryErr, searchQueryResult) =>{
				if(searchQueryErr) {
					res.status(400).send(searchQueryErr);
					client.end();

					return;
				}

				//get count of found cases
				client.query(searchQueries.countOfRecordsQueryStr, (countErr, countResult) => {
					if(countErr){
						res.status(400).send(searchQueryErr);
						client.end();

						return;
					}
					const searchResults = {records : searchQueryResult.rows, count : countResult.rows[0].count};

					//send cases and their quantity
					res.status(200).send(searchResults);
					client.end();
				})
			})
    	})
    }
});


app.post("/add-columns", jsonParser, (req, res)=>{
  	if(req.header('Authorization') != secToken) {
  		res.status(200).send('Non Authorized');
  		return;
  	}
    
    //connect to DB Postgres
    const { Client } = require('pg');
    const client = new Client({
    	connectionString: connectionString,
    	ssl: true
    });
    client.connect();
    
    var bodyRequest = req.body;
    var fieldsNameFromSF = [];
    var fieldsTypesFromSFMap = {};

    //get SF fields name and create map SF fieldName=>fieldType
    bodyRequest.forEach(element=>{
    	fieldsNameFromSF.push(element.fieldName)
    	fieldsTypesFromSFMap[element.fieldName] = element.fieldType;
    })

    var queryStr = "SELECT column_name FROM information_schema.columns WHERE table_name='cases'";

    //get all columns name in Postgres
    client.query(queryStr, (colsFromDBErr, colsFromDBResult) => {
    	var newColumns = getNewColumns(colsFromDBResult.rows, fieldsNameFromSF);

    	if(colsFromDBErr){
        	res.status(400).send('error in query columns');
        	client.end();
      	} else if(newColumns.length > 0) {
      		//create new columns in Postgres
        	var newColumnsQueryStr = createAddColsQueryStr(newColumns, fieldsTypesFromSFMap);

        	//add new columns in Postgrest
        	client.query(newColumnsQueryStr, (newColsError, newColsResult) => {
          		if(newColsError){
            		res.status(400).send('error in creating new columns');
            		client.end();
            		return;
          		}
          		res.status(200).send('New columns was created');
          		client.end();
        	})
      	} else {
        	res.status(200).send('no new columns');
        	client.end();
      	}
    })
})


app.post("/insert-records", jsonParser, (req, res)=>{
	if(req.header('Authorization') != secToken) {
	  	res.status(200).send('Non Authorized');
	  	return;
	}
    
    //connect to DB Postgres
    const { Client } = require('pg');
    const client = new Client({
    	connectionString: connectionString,
    	ssl: true
    });
    client.connect();

    var recordsFromSF = clearRecords(req.body['cases']);
    var columnsFromRequest = req.body['fields'];

    var insertRecordsQueryStr = createInsertRecordsQueryStr(columnsFromRequest, recordsFromSF);
    
    //insert cases into Postgres
    client.query(insertRecordsQueryStr, (insertRecErr, insertRecResult) => {
  		if(insertRecErr){
        	res.status(400).send(insertRecordsQueryStr);
      	} else {
        	res.status(200).send('New records added');
      	}
      	client.end();
    });
})


function getNewColumns(columnsFromDB, columnsFromRequest){
  	var columnsInPostgre = [];
  	var columnsToCreate = [];

  	columnsFromDB.forEach(el => {
    	columnsInPostgre.push(el.column_name);
  	})

  	columnsFromRequest.forEach(el => {
    	if(!columnsInPostgre.includes(el.toLowerCase())) {
      		columnsToCreate.push(el);
    	}
  	})

  	return columnsToCreate;
}

function createAddColsQueryStr(newColumns, fieldsTypesFromSFMap){
  	var newColumnsQuery = 'ALTER TABLE cases';

  	newColumns.forEach((col, index) =>{
	    var columnType = '';

	    switch(fieldsTypesFromSFMap[col]){
	  		case 'textarea':
		        columnType = 'TEXT';
		        break;
	   		case 'date':
		        columnType = 'DATE';
		        break;
	    	case 'time':
		        columnType = 'TIME';
		        break;
	    	case 'boolean':
		        columnType = 'BOOLEAN';
		        break;
		    case 'currency':
		    case 'double':
		    case 'percent':
		        columnType = 'FLOAT(3)';
		        break;
		    case 'timestamp':
		        columnType = 'TIMESTAMP';
		        break;
		    default:
		        columnType = 'VARCHAR (255)';
	    }

	    newColumnsQuery += ' ADD COLUMN ' + col + ' ' + columnType;

	    if(index < newColumns.length-1){
	    	newColumnsQuery += ',';
	    } else {
	    	newColumnsQuery += ';';
	    }
  	})

	return newColumnsQuery;
}

function createInsertRecordsQueryStr(columnsFromRequest, recordsFromSF){
  	var insertRecordsQuery = "INSERT INTO cases (";
  	columnsFromRequest.forEach((el, index) => {
    	insertRecordsQuery += el.toLowerCase();
		if(index < columnsFromRequest.length-1) {
      		insertRecordsQuery += ", ";
    	} else {
      		insertRecordsQuery += ")";
    	}
  	})

  	insertRecordsQuery+= " VALUES ";

  	for(var i = 0; i < recordsFromSF.length; i++){
    	insertRecordsQuery += "(";

	    for(var j = 0; j < columnsFromRequest.length; j++){
		  	var curFieldFromSF = columnsFromRequest[j];
	      	var fieldValue = recordsFromSF[i][curFieldFromSF];
	      	if(fieldValue === null || fieldValue === undefined){
	        	insertRecordsQuery += "null";
	      	} else {
	        	insertRecordsQuery += "'" + fieldValue + "'";
	      	}

	      	if(j < columnsFromRequest.length - 1){
	        	insertRecordsQuery += ", ";
	     	}
	    }

    	insertRecordsQuery += ")";

    	if(i < recordsFromSF.length-1){
      		insertRecordsQuery += ", ";
    	}
  	}
  	insertRecordsQuery += ";";

 	return insertRecordsQuery;
}

function clearRecords(recordsFromSF) {
  	recordsFromSF.forEach(el => {
      	if('attributes' in el) {
        	delete el['attributes'];
      	}
    })

  	return recordsFromSF;
}

function createSearchRecordsQueriesStr(textFields, search, offset, limit){
	var searchQueryStr = "SELECT * FROM cases WHERE ";
	var countOfRecordsQuery = "SELECT Count(*) FROM cases WHERE ";

	for(var i = 0; i < textFields.length; i++){
		searchQueryStr += textFields[i] + " ILIKE '%" + search + "%'";
		countOfRecordsQuery += textFields[i] + " ILIKE '%" + search + "%'";

		if (i < textFields.length - 1) {
			searchQueryStr += " OR ";
			countOfRecordsQuery += " OR ";
		}

	}
	searchQueryStr += " OFFSET " + offset + " LIMIT " + limit + ";";
	countOfRecordsQuery += ";";
	var searchQueries = {searchQueryStr:searchQueryStr, countOfRecordsQueryStr: countOfRecordsQuery};

	return searchQueries;
}